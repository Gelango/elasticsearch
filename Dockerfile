#####################################
# Multistage build for ElasticSearch
#####################################

######################################
# Stage 0
#   Download elasticsearch and xpack
#   Copy Configs
######################################
FROM registry.access.redhat.com/rhel:latest as prep_es_files

ENV ELK_VERSION 6.2.4


ENV no_proxy='localhost,127.0.0.1,aexp.com' \
    ftp_proxy='http://proxy.aexp.com:8080' \
    http_proxy='http://proxy.aexp.com:8080' \
    https_proxy='http://proxy.aexp.com:8080' \
    NO_PROXY='localhost,127.0.0.1,aexp.com' \
    FTP_PROXY='http://proxy.aexp.com:8080' \
    HTTP_PROXY='http://proxy.aexp.com:8080' \
    HTTPS_PROXY='http://proxy.aexp.com:8080'



ENV PATH /usr/share/elasticsearch/bin:$PATH
ENV JAVA_HOME /usr/lib/jvm/jre-1.8.0-openjdk
ADD repos /etc/yum.repos.d
RUN yum update -y && \
yum install -y java-1.8.0-openjdk-headless unzip which

RUN groupadd -g 1000 elasticsearch && \
    adduser -u 1000 -g 1000 -d /usr/share/elasticsearch elasticsearch

WORKDIR /usr/share/elasticsearch

USER 1000

# Download and extract defined ES version.

RUN curl -fsSL https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-${ELK_VERSION}.tar.gz | \
    tar zx --strip-components=1

RUN set -ex && for esdirs in config data logs; do \
        mkdir -p "$esdirs"; \
    done

# Download x-pack
RUN for PLUGIN in x-pack; do /usr/bin/curl -fsSL  https://artifacts.elastic.co/downloads/packs/x-pack/"$PLUGIN"-${ELK_VERSION}.zip >  "$PLUGIN"-${ELK_VERSION}.zip; elasticsearch-plugin install --batch file:///usr/share/elasticsearch/"$PLUGIN"-${ELK_VERSION}.zip; done

RUN for PLUGIN in ingest-user-agent ingest-geoip; do /usr/bin/curl -fsSL  https://artifacts.elastic.co/downloads/elasticsearch-plugins/"$PLUGIN"/"$PLUGIN"-${ELK_VERSION}.zip >  "$PLUGIN"-${ELK_VERSION}.zip; elasticsearch-plugin install --batch file:///usr/share/elasticsearch/"$PLUGIN"-${ELK_VERSION}.zip; done

COPY --chown=1000:0 elasticsearch.yml log4j2.properties config/

# Enable security logging audit trail
COPY --chown=1000:0 x-pack/log4j2.properties config/x-pack/
#RUN echo 'xpack.license.self_generated.type: trial' >>config/elasticsearch.yml`

USER 0

# Set gid to 0 for elasticsearch and make group permission similar to that of user
# This is needed, for example, for Openshift Open: https://docs.openshift.org/latest/creating_images/guidelines.html
# and allows ES to run with an uid
RUN chown -R elasticsearch:0 . && \
    chmod -R g=u /usr/share/elasticsearch

# Stage 1
FROM registry.access.redhat.com/rhel:latest

USER root
ENV no_proxy='localhost,127.0.0.1,aexp.com' \
    ftp_proxy='http://proxy.aexp.com:8080' \
    http_proxy='http://proxy.aexp.com:8080' \
    https_proxy='http://proxy.aexp.com:8080' \
    NO_PROXY='localhost,127.0.0.1,aexp.com' \
    FTP_PROXY='http://proxy.aexp.com:8080' \
    HTTP_PROXY='http://proxy.aexp.com:8080' \
    HTTPS_PROXY='http://proxy.aexp.com:8080'

ADD repos /etc/yum.repos.d


ENV ELASTIC_CONTAINER true
ENV PATH /usr/share/elasticsearch/bin:$PATH
ENV JAVA_HOME /usr/lib/jvm/jre-1.8.0-openjdk

RUN yum update -y && \
    yum install -y nc java-1.8.0-openjdk-headless unzip wget which && \
    yum clean all


RUN groupadd -g 1000 elasticsearch && \
    adduser -u 1000 -g 1000 -G 0 -d /usr/share/elasticsearch elasticsearch && \
    chmod 0775 /usr/share/elasticsearch && \
    chgrp 0 /usr/share/elasticsearch

WORKDIR /usr/share/elasticsearch

COPY --from=prep_es_files --chown=1000:0 /usr/share/elasticsearch /usr/share/elasticsearch

COPY --chown=1000:0 bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

# Openshift overrides USER and uses ones with randomly uid>1024 and gid=0
# Allow ENTRYPOINT (and ES) to run even with a different user
RUN chgrp 0 /usr/local/bin/docker-entrypoint.sh && \
    chmod g=u /etc/passwd && \
    chmod 0775 /usr/local/bin/docker-entrypoint.sh

EXPOSE 9200 9300

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["eswrapper"]